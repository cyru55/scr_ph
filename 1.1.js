// ==UserScript==
// @name         PH Downloader
// @version      1.1
// @description  download videos
// @author       cyru55
// @match        https://*.pornhub.com/view_video.php*
// @match        https://*.pornhub.com/embed/*
// @run-at       document-end
// @grant        none
// @license      WTFPL
// @downloadURL  https://gitlab.com/cyru55/scr_ph/-/raw/one/latest.js
// @updateURL    https://gitlab.com/cyru55/scr_ph/-/raw/one/latest.js
// ==/UserScript==

function O2S(n){var o=[];if(null==n)return String(n);if("object"==typeof n&&null==n.join){for(prop in n)n.hasOwnProperty(prop)&&o.push(prop+": "+O2S(n[prop]));return"{"+o.join(",")+"}"}if("object"==typeof n&&null!=n.join){for(prop in n)o.push(O2S(n[prop]));return"["+o.join(",")+"]"}return"function"==typeof n?o.push(n.toString()):o.push(JSON.stringify(n)),o.join(",")}

const Cy = {
	el:{
		id: str => document.getElementById(str),
		cls: str => document.getElementsByClassName(str),
		q: str => document.querySelectorAll(str),
		q1: str => document.querySelector(str),
		del: el => {
			if(typeof(el)=="string") el=Cy.el.id(el);
			if(el) el.parentNode.removeChild(el);
		}
	},
	block_adblock_blockers: ()=>{
		if(typeof(page_params)!="undefined")
			page_params.isAdBlockEnable=false;
		if(typeof(setCookieAdvanced)=="function")
			setCookieAdvanced('adBlockAlertHidden',1,1);
		Cy.el.del("abAlert");
	},
	to_list: function(obj,json){
		var out="";
		if(json.length){
			var title=obj.video_title
				.replace(/'s/ig,"")
				.replace(/[^\w\s\-\+\.,]/g,"")
				.replace(/([\-\+\.,]){2,}/g,"$1")
				.replace(/\s+/g," ")
				.replace(/^[\-\+\.\s_,]+/,"")
				.replace(/[\-\+\.\s_,]+$/,"");
			out = '<div style="text-align:center;"><a href="javascript:(()=>{ cymod.style.display=\'none\'; })();" style="color:#f00;font-size:99px;">x</a></div>';
			var loc = window.location.href;
			if(loc.indexOf("embed/")!=-1){
				out+='<div><a target="_blank" href="'+loc.replace("embed/","view_video.php?viewkey=")+'">🟡 High Resolution in New Tab</a></div><br>';
			}
			for(var i in json){
				if(json[i].format=="mp4"){
					out+='<div>'+
						'<a target="dl" download="'+title+'.mp4" href="'+json[i].videoUrl+'">⬇️ '+json[i].quality+'</a>'+
						'<button onclick="(()=>{ try{navigator.clipboard.writeText(\''+json[i].videoUrl+'\');}catch(e){} })();" style="margin-left:32px;padding:8px;">📎</button>'+
					'</div>';
				}
			}
			out+='<br><input type="text" value="'+title+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
		}
		return out;
	},
	make_modal: function(obj,json){
		var html = Cy.to_list(obj,json);
		if(html.length){
			var div1 = document.createElement("div");
			div1.id = "cymod";
			div1.style.cssText =
				'position:fixed;'+
				'z-index:999;'+
				'top:50%;'+
				'left:50%;'+
				'transform:translate(-50%,-70%);'+
				'width:320px;'+
				'width:60vw;'+
				'background:#000;'+
				'background:#000c;'+
				'padding:40px;'+
				'padding-top:0;'+
				'color:blue;'+
				'border-radius:44px;'+
				'box-shadow:0 0 44px 22px #000;'+
				'font-size:32px;';
			div1.innerHTML = html;
			document.body.appendChild(div1);
		}
	},
	handler: function(obj){
		for(var i in obj.mediaDefinitions){
			if(obj.mediaDefinitions[i].format=="mp4"){
				fetch(
					obj.mediaDefinitions[i].videoUrl,
					{mode:"cors"}
				)
				.then(res => {
					if(res.ok)
						return res.json();
					throw("server returns non2xx");
				})
				.then(json => {
					Cy.make_modal(obj,json);
				})
				.catch(err => {
					alert("Error while api request\n\n"+err);
				});
				break;
			}
		}
	}
};


var vid = 0;
var vars = null;
if(typeof(VIDEO_SHOW)!="undefined")
	vid = ~~(VIDEO_SHOW.trackVideoId||VIDEO_SHOW.video_id);
if(vid>0){
	console.log("vid: "+vid);
	if(window.hasOwnProperty("flashvars_"+vid)){
		vars = window["flashvars_"+vid];
	}
}
if(vars==null){// try find it in window scope
	for(var k in window) if(k.startsWith("flashvars")) vars=window[k];
}
if(vars!=null){
	console.log("vars: "+JSON.stringify(vars));
	if(vars.hasOwnProperty("mediaDefinitions")){
		Cy.handler(vars);
	}else alert("'flashvars' found but not contains 'mediaDefinitions'");
}else{// sandbox?
	var ta=document.createElement("textarea");
	ta.id="cyru55";
	ta.value="const Cy = "+O2S(Cy)+`;

		var vid = 0;
		var vars = null;
		if(typeof(VIDEO_SHOW)!="undefined")
			vid = ~~(VIDEO_SHOW.trackVideoId||VIDEO_SHOW.video_id);
		if(vid>0){
			console.log("vid: "+vid);
			if(window.hasOwnProperty("flashvars_"+vid)){
				vars = window["flashvars_"+vid];
			}
		}
		if(vars==null){// try find it in window scope
			for(var k in window) if(k.startsWith("flashvars")) vars=window[k];
		}
		if(vars!=null){
			console.log("vars: "+JSON.stringify(vars));
			if(vars.hasOwnProperty("mediaDefinitions")){
				Cy.handler(vars);
			}else alert("'flashvars' found but not contains 'mediaDefinitions'");
		}else alert("'flashvars' not found");
		Cy.el.del("cyru55");
	`;
	document.body.appendChild(ta);
	document.body.setAttribute("onload",'window.eval(document.getElementById("cyru55").value);');
}
// maximize view with removing right panel
Cy.el.del("hd-rightColVideoPage");
var el=Cy.el.id("vpContentContainer");
if(el) el.style.display="block";

for(var i=16;i;) setTimeout( ()=>Cy.block_adblock_blockers(), 200*--i);
