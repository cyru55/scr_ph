// ==UserScript==
// @name         x Downloader
// @version      6.3
// @description  download bad videos
// @author       https://t.me/cyru55
// @namespace    cyru55_x
// @match        https://*.pornhub.com/*
// @match        https://xhamster.com/*
// @match        https://spankbang.com/*
// @match        https://*.tnaflix.com/*
// @run-at       document-end
// @unwrap       x
// @sandbox      raw
// @grant        unsafeWindow
// @grant        GM_xmlhttpRequest
// @grant        GM_xmlHttpRequest
// @grant        GM.xmlHttpRequest
// @grant        GM_setClipboard
// @license      WTFPL
// @connect      pornhub.com
// @connect      pornhub.org
// @connect      phncdn.com
// @updateURL    https://gitlab.com/cyru55/scr_ph/-/raw/one/meta.js
// @downloadURL  https://gitlab.com/cyru55/scr_ph/-/raw/one/run.user.js
// ==/UserScript==

var Win = typeof(unsafeWindow)=="undefined"?window:unsafeWindow;
var Doc = Win.document;
let q = Win.location.host.split(".");

function O2S(n){let o=[];if(null==n)return String(n);if("object"==typeof n&&null==n.join){for(prop in n)n.hasOwnProperty(prop)&&o.push(prop+": "+O2S(n[prop]));return"{"+o.join(",")+"}"}if("object"==typeof n&&null!=n.join){for(prop in n)o.push(O2S(n[prop]));return"["+o.join(",")+"]"}return"function"==typeof n?o.push(n.toString()):o.push(JSON.stringify(n)),o.join(",")}

const Cy = {
	is: q[q.length-2]=="xhamster"?"XH":q[q.length-2]=="spankbang"?"SB":q[q.length-2]=="tnaflix"?"TN":"PH",
	el:{
		id: str => Doc.getElementById(str),
		cls: str => Doc.getElementsByClassName(str),
		q: str => Doc.querySelectorAll(str),
		q1: str => Doc.querySelector(str),
		del: el => {
			if(typeof(el)=="string") el=Cy.el.id(el);
			if(el) el.parentNode.removeChild(el);
		}
	},
	name_gen: str=>{
		if(typeof(str)!="string"||str.length<1) return "";
		let title = str
			.replace(/'s/ig,"")
			.replace(/[^\w\s\-\+\.,]/g,"")
			.replace(/([\-\+\.,]){2,}/g,"$1")
			.replace(/\s+/g," ")
			.replace(/^[\-\+\.\s_,]+/,"")
			.replace(/[\-\+\.\s_,]+$/,"")
			.replace(/\W*S\d+E\d+\W*/,"");
		return title.substr(0,232);
	},
	fetch: (url,cb,obj)=>{
		if(typeof(GM_xmlhttpRequest)!="undefined"){
			console.warn("--------------- fetch: GM_xmlhttpRequest");
			GM_xmlhttpRequest({
				method:"GET",
				url:url,
				onabort:()=>{ alert("GM_xmlhttpRequest>onabort"); },
				onerror:()=>{ alert("GM_xmlhttpRequest>onerror"); },
				ontimeout:()=>{ alert("GM_xmlhttpRequest>ontimeout"); },
				onload:function(r){
					let txt=r.responseText;
					console.log(">fetch "+txt);
					cb(txt,obj);
				}
			});
		}else if(typeof(GM)!="undefined" && typeof(GM.xmlHttpRequest)!="undefined"){
			console.warn("--------------- fetch: GM.xmlHttpRequest");
			GM.xmlHttpRequest({
				method:"GET",
				url:url,
				onabort:()=>{ alert("GM.xmlHttpRequest>onabort"); },
				onerror:()=>{ alert("GM.xmlHttpRequest>onerror"); },
				ontimeout:()=>{ alert("GM.xmlHttpRequest>ontimeout"); }
			})
			.then( res=>{
				let txt=res.responseText;
				console.log(">fetch "+txt);
				cb(txt,obj);
			})
			.catch( err=>{
				alert("Error while GM.xmlHttpRequest\n\n"+err);
			});
		}else{
			console.warn("--------------- fetch: fetch");
			fetch(url,{mode:"cors"})
				.then( res=>{
					if(res.ok)
						return res.text();
					throw("server returns non2xx");
				})
				.then( txt=>{
					console.log(">fetch "+txt);
					cb(txt,obj);
				})
				.catch( err=>{
					alert("Error while api request\n\n"+err);
				});
			}
	},
	ph_block_adblock_blockers: ()=>{
		if(typeof(page_params)!="undefined")
			page_params.isAdBlockEnable=false;
		if(typeof(setCookieAdvanced)=="function")
			setCookieAdvanced("adBlockAlertHidden",1,1,"/","pornhub.com");
		Cy.el.del("abAlert");
		if(typeof(MG_Utils)!="undefined")
			if(header=Cy.el.id("header"))
				MG_Utils.removeClass(header,"hasAdAlert");
		page_params.isAdBlockEnable=false;
	},
	ph_rm_age_nag: ()=>{
		Cy.el.del("ageDisclaimerMainBG");
		Cy.el.del("ageDisclaimerOverlay");
		Cy.el.del("js-ageDisclaimerModal");
		Cy.el.del("modalWrapMTubes");
		if(typeof(setCookieAdvanced)=="function")
			setCookieAdvanced("accessAgeDisclaimerPH",1,1,"/","pornhub.com");
	},
	ph_change_params: ()=>{
		for(let i=32;i;){
			setTimeout( ()=>{
				if(typeof(essentialCookiesListAll)!="undefined"){
					essentialCookiesListAll["pornhub.com"]["CookieConsent"]=3;
					essentialCookiesListAll["pornhub.com"]["cookieConsent"]=3;
					essentialCookiesListAll["pornhub.com"]["accessAgeDisclaimerPH"]=1;
					essentialCookiesListAll["pornhub.com"]["accessAgeDisclaimerUK"]=1;
					essentialCookiesListAll["pornhub.com"]["accessPH"]=1;
					essentialCookiesListAll["pornhub.com"]["adBlockAlertHidden"]=1;
					essentialCookiesListAll["pornhub.com"]["age_verified"]=1;
					essentialCookiesListAll["pornhub.com"]["cookieBannerEU"]=1;
					essentialCookiesListAll["pornhub.com"]["cookieBannerState"]=1;
					essentialCookiesListAll["pornhub.com"]["cookiesBanner"]=1;
					essentialCookiesListAll["pornhub.com"]["cookiesBannerSeen"]=1;
					essentialCookiesListAll["pornhub.com"]["autoplay"]=0;
				}
			},500*--i);
		}
	},
	ph_to_list: (obj,src)=>{
		let out="";
		if(src.length){
			let title = Cy.name_gen(obj.video_title);
			out = '<div style="text-align:center;"><a href="javascript:(()=>{ cymod.style.display=\'none\'; })();" style="color:#f00;font-size:99px;">x</a></div>';
			if(Win.location.href.indexOf("embed/")!=-1){
				out+='<div><a target="_blank" href="'+Win.location.href.replace("embed/","view_video.php?viewkey=")+'">🟡 High Resolution in New Tab</a></div><br>';
			}
			let vhex = Win.location.href.substr(Win.location.href.indexOf("=")+1);
			for(let i in src){
				if(src[i].format=="mp4"){
					out+='<div>'+
						'<a target="dl" download="'+title+(vhex.length>3?" ,PH"+vhex:"")+'.mp4" href="'+src[i].videoUrl+'">⬇️ '+src[i].quality+'</a>'+
						'<button onclick="(()=>{ if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(\''+src[i].videoUrl+'\',\'text\');else try{navigator.clipboard.writeText(\''+src[i].videoUrl+'\');}catch(e){} })();" style="margin-left:32px;padding:8px;cursor:pointer">📎</button>'+
					'</div>';
				}
			}
			if(vhex.length>3){
				out+='<br><input type="text" value="PH_'+vhex+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(_.value,\'text\');else try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
				out+='<br><input type="text" value="'+title+(vhex.length>3?" ,PH"+vhex:"")+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(_.value,\'text\');else try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
			}
		}
		return out;
	},
	xh_to_list: (obj,src)=>{
		let out="";
		let title = Cy.name_gen(obj?.videoTitle?.pageTitle??"");
		out = '<div style="text-align:center;"><a href="javascript:(()=>{ cymod.style.display=\'none\'; })();" style="color:#f00;font-size:99px;">x</a></div>';
		let vid = Win.location.href.slice(Win.location.href.lastIndexOf("-")+1);
		for(let k in src){
			let web = obj?.xhlMlSource?.payload?.site?? obj?.collectorData?.site;
			if(web=="mobile"){
				let link = src[k]?.label?? src[k]?.quality?? "Click";
				if(link!="auto"){
					out+='<div>'+
						'<a target="dl" style="color:#ff0" download="'+title+(vid.length>3?" ,XH"+vid:"")+'.mp4" href="'+src[k].url+'">⬇️ '+link+'</a>'+
						'<button onclick="(()=>{ if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(\''+src[k].url+'\',\'text\');else try{navigator.clipboard.writeText(\''+src[k].url+'\');}catch(e){} })();" style="margin-left:32px;padding:8px;cursor:pointer">📎</button>'+
					'</div>';
				}
			}else{// desktop
				out+='<div>'+
					'<a target="dl" style="color:#ff0" download="'+title+(vid.length>3?" ,XH"+vid:"")+'.mp4" href="'+src[k]+'">⬇️ '+k+'</a>'+
					'<button onclick="(()=>{ if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(\''+src[k]+'\',\'text\');else try{navigator.clipboard.writeText(\''+src[k]+'\');}catch(e){} })();" style="margin-left:32px;padding:8px;cursor:pointer">📎</button>'+
				'</div>';
			}
		}
		if(vid.length>3){
			out+='<br><input type="text" value="XH_'+vid+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(_.value,\'text\');else try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
			out+='<br><input type="text" value="'+title+(vid.length>3?" ,XH"+vid:"")+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(_.value,\'text\');else try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
		}
		return out;
	},
	sb_to_list: (name,src)=>{
		let out="";
		let title = Cy.name_gen(name);
		out = '<div style="text-align:center;"><a href="javascript:(()=>{ cymod.style.display=\'none\'; })();" style="color:#f00;font-size:99px;">x</a></div>';
		let vid = Win.location.pathname.split("/")[1];
		for(let k in src){
			if(
				["240p","320p","480p","720p","1080p","4k"].includes(k)&&
				src[k].length
			){
				out+='<div>'+
					'<a target="dl" style="color:#ff0" download="'+title+(vid.length?" ,SB"+vid:"")+'.mp4" href="'+src[k][0]+'">⬇️ '+k+'</a>'+
					'<button onclick="(()=>{ if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(\''+src[k][0]+'\',\'text\');else try{navigator.clipboard.writeText(\''+src[k][0]+'\');}catch(e){} })();" style="margin-left:32px;padding:8px;cursor:pointer">📎</button>'+
				'</div>';
			}
		}
		if(vid.length>3){
			out+='<br><input type="text" value="SB_'+vid+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(_.value,\'text\');else try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
			out+='<br><input type="text" value="'+title+(vid.length>3?" ,SB"+vid:"")+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(_.value,\'text\');else try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
		}
		return out;
	},
	tn_to_list: (obj)=>{
		let out="";
		let title = Cy.name_gen(obj?.name);
		out = '<div style="text-align:center;"><a href="javascript:(()=>{ cymod.style.display=\'none\'; })();" style="color:#f00;font-size:99px;">x</a></div>';
		for(let link of obj.vids){
			let pos=link.lastIndexOf(".");let resolution=link.substring(pos-5,pos).replace("-","");
			out+='<div>'+
				'<a target="dl" style="color:#ff0" download="'+title+(obj?.vid.length?" ,TN"+obj?.vid:"")+'.mp4" href="'+link+'">⬇️ '+resolution+'</a>'+
				'<button onclick="(()=>{ if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(\''+link+'\',\'text\');else try{navigator.clipboard.writeText(\''+link+'\');}catch(e){} })();" style="margin-left:32px;padding:8px;cursor:pointer">📎</button>'+
			'</div>';
		}
		if(obj?.vid.length>3){
			out+='<br><input type="text" value="TN_'+obj?.vid+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(_.value,\'text\');else try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
			out+='<br><input type="text" value="'+title+(obj?.vid.length>3?" ,TN"+obj?.vid:"")+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();if(typeof(GM_setClipboard)!=\'undefined\') GM_setClipboard(_.value,\'text\');else try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
		}
		return out;
	},
	make_modal: (obj,src)=>{
		console.warn("--------------- make_modal");
		let html =
			Cy.is=="PH"? Cy.ph_to_list(obj,src):
			Cy.is=="XH"? Cy.xh_to_list(obj,src):
			Cy.is=="TN"? Cy.tn_to_list(obj):
			Cy.sb_to_list(obj,src);// SB
		if(html.length){
			let top_pop = document.createElement("div");
			top_pop.id = "cymod";
			top_pop.style.cssText =
				'position:fixed;'+
				'z-index:999;'+
				'top:50%;'+
				'left:50%;'+
				'transform:translate(-50%,-70%);'+
				'width:320px;'+
				'width:60vw;'+
				'background:#000;'+
				'background:#000c;'+
				'padding:0 40px 40px;'+
				'border-radius:44px;'+
				'box-shadow:0 0 44px 22px #000;'+
				'font-size:24px;';
			top_pop.innerHTML = html;
			document.body.appendChild(top_pop);
			// hide modal in dl btn
			if(Cy.is=="PH"){
				let is_mobile= typeof(isPlatformMobile)!="undefined"&&isPlatformMobile==1 || typeof(FLAG_COMMENT_DATA)!="undefined"&&FLAG_COMMENT_DATA.platform=="mobile";
				let el=Cy.el.id(is_mobile?"flagVideoBtn":"video_flag");
				if(el){
					let dl_btn = document.createElement("button");
					dl_btn.id = "btn_dl";
					dl_btn.className = "reportBtn buttonClass generalBtn";
					dl_btn.style.cssText="background:#723;animation:cyglow 2s infinite;";
					if(!is_mobile) dl_btn.style.cssText+="padding:7px;";
					dl_btn.innerHTML='<i class="ph-icon-arrow-down"></i><span>Download</span>';
					dl_btn.onclick=()=>{ top_pop.style.display="block"; };
					el.parentNode.insertBefore(dl_btn, el.nextSibling);
					Cy.el.del(el);
					top_pop.style.display="none";
				}
			}
		}
	},
	handler: obj=>{
		console.warn("--------------- handler");
		if(Cy.is=="PH"){
			for(let i in obj.mediaDefinitions){
				if(obj.mediaDefinitions[i].format=="mp4"){
					console.warn("---------------> "+obj.mediaDefinitions[i].videoUrl);
					let str=Cy.fetch(
						obj.mediaDefinitions[i].videoUrl,
						(t,o)=>{
							console.log(">cb "+t);
							if(t.length){
								let json={};
								try{ json=JSON.parse(t); }catch(e){}
								console.warn(json);
								if(Object.keys(json).length)
									Cy.make_modal(o,json);
							}
						},
						obj
					);
					break;
				}
			}
		}else if(Cy.is=="XH"){
			Cy.make_modal(Win.initials, obj);
		}else if(Cy.is=="SB"){
			let name = "";
			let ld = document.querySelectorAll('script[type="application/ld+json"]')[0].textContent;
			if(ld){
				let json = {};
				try{
					json = JSON.parse(ld);
					if(Object.keys(json).length){
						name = json.name;
					}
				}catch(e){}
			}
			Cy.make_modal(name, obj);
		}else if(Cy.is=="TN"){
			Cy.make_modal(obj);
		}
	}
};

console.warn("--------------- RUN script >"+Cy.is);

if(Cy.is=="PH"){
	if(Win.location.href.indexOf("embed/")>0||Win.location.href.indexOf("view_video.php")>0){
		let vid = 0;
		let vars = null;
		if(typeof(VIDEO_SHOW)!="undefined")
			vid = ~~(VIDEO_SHOW.trackVideoId||VIDEO_SHOW.video_id);
		if(typeof(Win.VIDEO_SHOW)!="undefined")
			vid = ~~(Win.VIDEO_SHOW.trackVideoId||Win.VIDEO_SHOW.video_id);
		if(vid>0){
			console.log("vid: "+vid);
			if(Win.hasOwnProperty("flashvars_"+vid)){
				vars = Win["flashvars_"+vid];
			}
		}
		if(vars==null){// try find it in window scope
			for(let k in Win) if(k.startsWith("flashvars")) vars=Win[k];
		}
		if(vars!=null){
			console.log("vars: "+JSON.stringify(vars));
			if(vars.hasOwnProperty("mediaDefinitions")){
				Cy.handler(vars);
			}else alert("'flashvars' found but not contains 'mediaDefinitions'");
		}else{// sandbox?
			let ta=Doc.createElement("textarea");
			ta.id="cyru55";
			ta.value="const Cy = "+O2S(Cy)+`;

				let vid = 0;
				let vars = null;
				if(typeof(VIDEO_SHOW)!="undefined")
					vid = ~~(VIDEO_SHOW.trackVideoId||VIDEO_SHOW.video_id);
				if(typeof(Win)!="undefined" && typeof(Win.VIDEO_SHOW)!="undefined")
					vid = ~~(Win.VIDEO_SHOW.trackVideoId||Win.VIDEO_SHOW.video_id);
				if(vid>0){
					console.log("vid: "+vid);
					if(window.hasOwnProperty("flashvars_"+vid)){
						vars = window["flashvars_"+vid];
					}
				}
				if(vars==null){// try find it in window scope
					for(let k in window) if(k.startsWith("flashvars")) vars=window[k];
				}
				if(vars!=null){
					console.log("vars: "+JSON.stringify(vars));
					if(vars.hasOwnProperty("mediaDefinitions")){
						Cy.handler(vars);
					}else alert("'flashvars' found but not contains 'mediaDefinitions'");
				}else alert("'flashvars' not found");
				Cy.el.del("cyru55");
			`;
			Doc.body.appendChild(ta);
			Doc.body.setAttribute("onload",'window.eval(document.getElementById("cyru55").value);');
		}
	}
	// maximize view with removing right panel
	Cy.el.del("hd-rightColVideoPage");
	let el=Cy.el.id("vpContentContainer");
	if(el) el.style.display="block";
	// bigger watched flag
	let style1 = Doc.createElement("style");
	style1.innerText = "div.watchedVideoText{line-height:32px;font-size:32px;} @keyframes cyglow{0%{box-shadow:0 0 0px #f00}50%{box-shadow:0 0 48px #f00}}";
	Doc.head.appendChild(style1);
	// remove adblock blockers
	for(let i=16;i;) setTimeout( ()=>Cy.ph_block_adblock_blockers(), 200*--i);
	// remove cookie banner
	Cy.el.del("cookieBanner");
	Doc.cookie = "cookieConsent=3; expires=0;domain=pornhub.com;secure;path=/";
	// set language = en
	if(typeof(setCookieAdvanced)!="undefined")
		setCookieAdvanced("lang","en", 30, "/", removeSubdomain(Win.location.host));
	// may set backend dont-show-ads
	if(typeof(MG_Utils)!="undefined")
		MG_Utils.ajaxCall({
			type: "GET",
			url: "/front/cookie_kill_ajax",
			data: {cookie_name: "showPremiumWelcomeFromPornhub"}
		});
	// try set custom settings
	Cy.ph_change_params();
	// remove overlay text ads
	setInterval(()=>{
		Cy.el.del(Cy.el.q1("#player div.mgp_overlayText"));
	},500);
	// remove age warning
	//Cy.ph_rm_age_nag();
}else if(Cy.is=="XH"){
	if(
		Win.hasOwnProperty("initials")&&
		!Win.location.pathname.indexOf("/videos/")
	){
		let mp4 = initials?.videoModel?.sources?.mp4?? initials?.xplayerSettings?.sources?.standard?.h264;
		if(mp4){
			Cy.handler(mp4);
		}else alert("it seems website scripts changed\ncontact me @cyru55");
		setInterval( ()=>{
			let ads = Doc.querySelectorAll("div.thumb-list>div:not(.thumb-list__item)");
			for(let ad of ads) Cy.el.del(ad);
		}, 200);
		let css = Doc.createElement("style");
		css.innerText = `
		.player-container{margin-right:unset !important}
		.thumb-image-container__watched{background:red !important;height:32px !important}
		.thumb-image-container__watched>div{font-size:26px}`;
		Doc.getElementsByTagName("head")[0].appendChild(css);
	}
}else if(Cy.is=="SB"){
	if(
		Win.location.pathname.length>1&&
		Win.hasOwnProperty("stream_data")
	){
		if(stream_data.length){
			Cy.handler(stream_data);
		}else alert("it seems website scripts changed\ncontact me @cyru55");
	}
	let styl = Doc.getElementsByTagName("style");
	styl[styl.length-1].innerText+="#cymod input{background:#323232}";
	Cy.el.del(Cy.el.q1("#video .right"));
}else if(Cy.is=="TN"){
	if(Win.location.pathname.length>1){
		Cy.i_tn1=setInterval(()=>{
			let vid_el=Cy.el.id("video-player");
			if(vid_el.children.length){
				clearInterval(Cy.i_tn1);
				let vid=~~vid_el.getAttribute("data-vid");
				let vids=[];
				for(let el of vid_el.children){
					let link=el.getAttribute("src")??"";
					if(link.length) vids.push(link);
				}
				let name = "";
				let ld = document.querySelectorAll('script[type="application/ld+json"]')[0].textContent;
				if(ld){
					let json = {};
					try{
						json = JSON.parse(ld);
						if(Object.keys(json).length){
							name = json.name;
						}
					}catch(e){}
				}
				console.warn({"vid":vid,"vids":vids,"name":name});
				if(vids.length)
					Cy.handler({"vid":vid+"","vids":vids,"name":name});
			}
		},500);
	}
}