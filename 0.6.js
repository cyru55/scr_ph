// ==UserScript==
// @name         PH Downloader
// @version      0.6.3
// @description  download videos
// @author       cyru55
// @match        https://*.pornhub.com/view_video.php*
// @inject-into  page
// @run-at       document-end
// @grant        unsafeWindow
// @grant        GM.setClipboard
// @unwrap
// ==/UserScript==

(function cy(once){

	if(typeof(window)=="undefined"){
		alert("'undefined'==window");
		return;
	}
	// Trick to bypass sandbox restrictions in Greasemonkey (Firefox)
	if(this!==window&&!once){
		window.eval("("+cy.toString()+")(true);");
		return;
	}
	var win=typeof(unsafeWindow)!="undefined"?unsafeWindow:window;

	var Cy = {
		el:{
			id: str => document.getElementById(str),
			cls: str => document.getElementsByClassName(str),
			q: str => document.querySelectorAll(str),
			q1: str => document.querySelector(str),
			del: el => {
				if(typeof(el)=="string") el=Cy.el.id(el);
				if(el) el.parentNode.removeChild(el);
			}
		},
		block_adblock_blockers: ()=>{// never used
			if(typeof(page_params)!="undefined")
				page_params.isAdBlockEnable=false;
			if(typeof(setCookieAdvanced)=="function")
				setCookieAdvanced('adBlockAlertHidden',1,1);
			Cy.el.del("abAlert");
		},
		copy_then_hide: function(url){
			if(typeof(GM)!="undefined")
				GM.setClipboard(url);
			setTimeout(()=>{
				Cy.el.del("cyru55");
			},500);
		},
		to_list: function(obj,json){
			var out="";
			if(json.length){
				out = '<div style="text-align:center;"><a href="javascript:Cy.el.del(\'cyru55\');" style="color:#f00;font-size:99px;">x</a></div>';
				for(var i in json){
					if(json[i].format=="mp4"){
						var title=obj.video_title.replace(/'s/g,"").replace(/[^\w\s\-\+\.,]/g,"").replace(/([\-\+\.,]){2,}/g,"$1").replace(/\s+/g," ");
						out+='<div><a target="dl" download="'+title+'.mp4" href="'+json[i].videoUrl+'" onclick="if(typeof(Cy)!=\'undefined\') Cy.copy_then_hide(\''+json[i].videoUrl+'\');">'+json[i].quality+'</a></div>';
					}
				}
			}
			return out;
		},
		make_modal: function(obj,json){
			var html = Cy.to_list(obj,json);
			if(html.length){
				var div = document.createElement('div');
				div.innerHTML =
					'<div id="cyru55" style="'+
						'position:fixed;'+
						'z-index:999;'+
						'top:50%;'+
						'left:50%;'+
						'transform:translate(-50%,-70%);'+
						'width:320px;'+
						'width:60vw;'+
						'background:#000;'+
						'background:#000c;'+
						'padding:40px;'+
						'padding-top:0;'+
						'color:blue;'+
						'border-radius:44px;'+
						'box-shadow:0 0 44px #000;'+
						'font-size:32px;'+
					'">'+html+'</div>';
				document.body.appendChild(div);
			}
		},
		handler: function(obj){
			for(var i in obj.mediaDefinitions){
				if(obj.mediaDefinitions[i].format=="mp4"){
					fetch(
						obj.mediaDefinitions[i].videoUrl,
						{mode:"cors"}
					)
					.then(res => {
						if(res.ok)
							return res.json();
						throw("server returns non2xx");
					})
					.then(json => {
						Cy.make_modal(obj,json);
					})
					.catch(err => {
						alert("Error while api request\n\n"+err);
					});
					break;
				}
			}
		}
	};

	// START
	var vid=0;
	if(typeof(VIDEO_SHOW)!="undefined"){
		vid = ~~(VIDEO_SHOW.trackVideoId||VIDEO_SHOW.video_id);
	}else{
		if(win.hasOwnProperty("VIDEO_SHOW")){
			vid = ~~win["VIDEO_SHOW"]["video_id"];
		}else{
			alert("It seems script have not access to the page contents\nmay\nyour browser is using an incorrect plugin\nor\nthe website has changed and this old script will no longer work");return;
		}
	}
	if(vid>0){
		var vars=null;
		if(win.hasOwnProperty("flashvars_"+vid)){
			vars=win["flashvars_"+vid];
		}else{// try find it in windows scope
			for(var k in win) if(k.startsWith("flashvars")) vars=win[k];
		}
		if(typeof(vars)!=null){
			if(vars.hasOwnProperty("mediaDefinitions")){
				Cy.handler(vars);
			}else{
				alert("`flashvars` found but not contains `mediaDefinitions`");return;
			}
		}else{
			alert("`vid` found but `flashvars` not");return;
		}
	}else{
		alert("Script cant find `vid`");return;
	}

	// maximize view with removing right panel
	Cy.el.del("hd-rightColVideoPage");
	Cy.el.id("vpContentContainer").style.display="block";

})(false);
