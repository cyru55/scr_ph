// ==UserScript==
// @name         PH Downloader
// @version      0.5
// @description  download videos
// @author       cyru55
// @match        https://*.pornhub.com/*
// @inject-into  page
// @grant        unsafeWindow
// @grant        GM.setClipboard
// @unwrap
// ==/UserScript==

(function(){

	window["Cy"] = {
		el:{
			id: str => document.getElementById(str),
			del: el => {if(typeof(el)=="string") el=Cy.el.id(el);el.parentNode.removeChild(el)}
		},
		block_adblock_blockers: function(){
			// hey PH, plz do not block "block ad-block blockers"
			if(typeof(page_params)!="undefined")
				page_params.isAdBlockEnable = false;
			if(typeof(setCookieAdvanced)=="function")
				setCookieAdvanced('adBlockAlertHidden',1,1);
			Cy.el.del("abAlert");
		},
		modal: function(obj,json){
			var html='<div style="text-align:center;"><a href="javascript:Cy.el.del(\'cyru55\');" style="color:#f00;font-size:99px;">x</a></div>';
			for(var i in json){
				if(json[i].format=="mp4"){
					var title=obj.video_title.replace(/'s/g,"").replace(/[^\w\s\-\+\.,]/g,"").replace(/([\-\+\.,]){2,}/g,"$1").replace(/\s+/g," ");
					html+='<div><a target="dl" download="'+title+'.mp4" href="'+json[i].videoUrl+'" onclick="(function(){if(typeof(GM)!="undefined") GM.setClipboard(\''+json[i].videoUrl+'\');setTimeout(function(){Cy.el.del(\'cyru55\');},500);})();">'+json[i].quality+'</a></div>';
				}
			}
			if(html.length){
				var div = document.createElement('div');
				div.innerHTML = '<div style="position:fixed;z-index:999;top:50%;left:50%;transform:translate(-50%,-70%);width:320px;width:60vw;background:#000;background:#000c;padding:40px;padding-top:0;color:blue;border-radius:44px;box-shadow:0 0 44px #000;font-size:32px;" id="cyru55">'+html+'</div>';
				document.body.appendChild(div);
			}
		},
		handler: function(obj){
			for(var i in obj.mediaDefinitions){
				if(obj.mediaDefinitions[i].format=="mp4"){
					fetch(
						obj.mediaDefinitions[i].videoUrl,
						{mode:"cors"}
					)
					.then(response => {
						if(response.ok && response.status==200){
							return response.json();
						}
					})
					.then( data => {
						Cy.modal(obj,data);
					})
					.catch(err => {
						alert("Error:\n"+err);
					});
					break;
				}
			}
		}
	};

	var i1 = setInterval( () => {

		try{
			Cy.block_adblock_blockers();
		}catch(e){}

		var vid=VIDEO_SHOW.trackVideoId||VIDEO_SHOW.video_id||window["VIDEO_SHOW"]["video_id"];
		var flashvars=window["flashvars_"+vid];
		if(typeof(flashvars)=="undefined"){
			for(k in window) if(k.startsWith("flashvars")) flashvars=window[k];
		}
		if(typeof(flashvars)!="undefined"){
			if(flashvars.hasOwnProperty("mediaDefinitions")){
				Cy.handler(flashvars);
				clearInterval(i1);
			}
		}

	},100);

	// maximize view with removing right panel
	Cy.el.del("hd-rightColVideoPage");
	Cy.el.id("vpContentContainer").style.display="block";

})();
