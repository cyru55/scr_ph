// ==UserScript==
// @name         PH/XH Downloader
// @version      2.4
// @description  download good videos
// @author       cyru55
// @match        https://*.pornhub.com/*
// @match        https://xhamster.com/*
// @run-at       document-end
// @grant        none
// @license      WTFPL
// @downloadURL  https://gitlab.com/cyru55/scr_ph/-/raw/one/latest.js
// @updateURL    https://gitlab.com/cyru55/scr_ph/-/raw/one/latest.js
// ==/UserScript==

function O2S(n){let o=[];if(null==n)return String(n);if("object"==typeof n&&null==n.join){for(prop in n)n.hasOwnProperty(prop)&&o.push(prop+": "+O2S(n[prop]));return"{"+o.join(",")+"}"}if("object"==typeof n&&null!=n.join){for(prop in n)o.push(O2S(n[prop]));return"["+o.join(",")+"]"}return"function"==typeof n?o.push(n.toString()):o.push(JSON.stringify(n)),o.join(",")}

let q = window.location.host.split(".");

const Cy = {
	is: q[q.length-2]=="xhamster"?"XH":"PH",
	G:{
		loc: window.location.href
	},
	el:{
		id: str => document.getElementById(str),
		cls: str => document.getElementsByClassName(str),
		q: str => document.querySelectorAll(str),
		q1: str => document.querySelector(str),
		del: el => {
			if(typeof(el)=="string") el=Cy.el.id(el);
			if(el) el.parentNode.removeChild(el);
		}
	},
	name_gen: str=>{
		if(typeof(str)!="string"||str.length<1) return "";
		let title = str
			.replace(/'s/ig,"")
			.replace(/[^\w\s\-\+\.,]/g,"")
			.replace(/([\-\+\.,]){2,}/g,"$1")
			.replace(/\s+/g," ")
			.replace(/^[\-\+\.\s_,]+/,"")
			.replace(/[\-\+\.\s_,]+$/,"")
			.replace(/\W*S\d+E\d+\W*/,"");
		return title.substr(0,232);
	},
	ph_block_adblock_blockers: ()=>{
		if(typeof(page_params)!="undefined")
			page_params.isAdBlockEnable=false;
		if(typeof(setCookieAdvanced)=="function")
			setCookieAdvanced("adBlockAlertHidden",1,1,"/","pornhub.com");
		Cy.el.del("abAlert");
		if(typeof(MG_Utils)!="undefined")
			MG_Utils.removeClass(Cy.el.id("header"),"hasAdAlert");
		page_params.isAdBlockEnable=false;
	},
	ph_rm_age_nag: ()=>{
		Cy.el.del("ageDisclaimerMainBG");
		Cy.el.del("ageDisclaimerOverlay");
		Cy.el.del("js-ageDisclaimerModal");
		Cy.el.del("modalWrapMTubes");
		if(typeof(setCookieAdvanced)=="function")
			setCookieAdvanced("accessAgeDisclaimerPH",1,1,"/","pornhub.com");
	},
	ph_change_params: ()=>{
		for(let i=32;i;){
			setTimeout( ()=>{
				if(typeof(essentialCookiesListAll)!="undefined"){
					essentialCookiesListAll["pornhub.com"]["CookieConsent"]=3;
					essentialCookiesListAll["pornhub.com"]["cookieConsent"]=3;
					essentialCookiesListAll["pornhub.com"]["accessAgeDisclaimerPH"]=1;
					essentialCookiesListAll["pornhub.com"]["accessAgeDisclaimerUK"]=1;
					essentialCookiesListAll["pornhub.com"]["accessPH"]=1;
					essentialCookiesListAll["pornhub.com"]["adBlockAlertHidden"]=1;
					essentialCookiesListAll["pornhub.com"]["age_verified"]=1;
					essentialCookiesListAll["pornhub.com"]["cookieBannerEU"]=1;
					essentialCookiesListAll["pornhub.com"]["cookieBannerState"]=1;
					essentialCookiesListAll["pornhub.com"]["cookiesBanner"]=1;
					essentialCookiesListAll["pornhub.com"]["cookiesBannerSeen"]=1;
					essentialCookiesListAll["pornhub.com"]["autoplay"]=0;
				}
			},500*--i);
		}
	},
	ph_to_list: (obj,src)=>{
		let out="";
		if(src.length){
			let title = Cy.name_gen(obj.video_title);
			out = '<div style="text-align:center;"><a href="javascript:(()=>{ cymod.style.display=\'none\'; })();" style="color:#f00;font-size:99px;">x</a></div>';
			if(Cy.G.loc.indexOf("embed/")!=-1){
				out+='<div><a target="_blank" href="'+Cy.G.loc.replace("embed/","view_video.php?viewkey=")+'">🟡 High Resolution in New Tab</a></div><br>';
			}
			let vhex = Cy.G.loc.substr(Cy.G.loc.indexOf("=")+1);
			for(let i in src){
				if(src[i].format=="mp4"){
					out+='<div>'+
						'<a target="dl" download="'+title+(vhex.length>3?" ,PH"+vhex:"")+'.mp4" href="'+src[i].videoUrl+'">⬇️ '+src[i].quality+'</a>'+
						'<button onclick="(()=>{ try{navigator.clipboard.writeText(\''+src[i].videoUrl+'\');}catch(e){} })();" style="margin-left:32px;padding:8px;cursor:pointer">📎</button>'+
					'</div>';
				}
			}
			if(vhex.length>3){
				out+='<br><input type="text" value="PH_'+vhex+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
				out+='<br><input type="text" value="'+title+(vhex.length>3?" ,PH"+vhex:"")+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
			}
		}
		return out;
	},
	xh_to_list: (obj,src)=>{
		let out="";
		let title = Cy.name_gen(obj?.videoTitle?.pageTitle??"");
		out = '<div style="text-align:center;"><a href="javascript:(()=>{ cymod.style.display=\'none\'; })();" style="color:#f00;font-size:99px;">x</a></div>';
		let vid = Cy.G.loc.slice(Cy.G.loc.lastIndexOf("-")+1);
		for(let k in src){
			let web = obj?.xhlMlSource?.payload?.site?? obj?.collectorData?.site;
			if(web=="mobile"){
				let link = src[k]?.label?? src[k]?.quality?? "Click";
				if(link!="auto"){
					out+='<div>'+
						'<a target="dl" style="color:#ff0" download="'+title+(vid.length>3?" ,XH"+vid:"")+'.mp4" href="'+src[k].url+'">⬇️ '+link+'</a>'+
						'<button onclick="(()=>{ try{navigator.clipboard.writeText(\''+src[k].url+'\');}catch(e){} })();" style="margin-left:32px;padding:8px;cursor:pointer">📎</button>'+
					'</div>';
				}
			}else{// desktop
				out+='<div>'+
					'<a target="dl" style="color:#ff0" download="'+title+(vid.length>3?" ,XH"+vid:"")+'.mp4" href="'+src[k]+'">⬇️ '+k+'</a>'+
					'<button onclick="(()=>{ try{navigator.clipboard.writeText(\''+src[k]+'\');}catch(e){} })();" style="margin-left:32px;padding:8px;cursor:pointer">📎</button>'+
				'</div>';
			}
		}
		if(vid.length>3){
			out+='<br><input type="text" value="XH_'+vid+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
			out+='<br><input type="text" value="'+title+(vid.length>3?" ,XH"+vid:"")+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
		}
		return out;
	},
	make_modal: (obj,src)=>{
		console.warn("--------------- make_modal");
		let html = Cy.is=="PH"? Cy.ph_to_list(obj,src): Cy.xh_to_list(obj,src);
		if(html.length){
			let div1 = document.createElement("div");
			div1.id = "cymod";
			div1.style.cssText =
				'position:fixed;'+
				'z-index:999;'+
				'top:50%;'+
				'left:50%;'+
				'transform:translate(-50%,-70%);'+
				'width:320px;'+
				'width:60vw;'+
				'background:#000;'+
				'background:#000c;'+
				'padding:0 40px 40px;'+
				'border-radius:44px;'+
				'box-shadow:0 0 44px 22px #000;'+
				'font-size:32px;';
			div1.innerHTML = html;
			document.body.appendChild(div1);
		}
	},
	handler: obj=>{
		console.warn("--------------- handler");
		if(Cy.is=="PH"){
			for(let i in obj.mediaDefinitions){
				if(obj.mediaDefinitions[i].format=="mp4"){
					fetch(
						obj.mediaDefinitions[i].videoUrl,
						{mode:"cors"}
					)
					.then( res=>{
						if(res.ok)
							return res.json();
						throw("server returns non2xx");
					})
					.then( json=>{
						console.log(json);
						Cy.make_modal(obj,json);
					})
					.catch( err=>{
						alert("Error while api request\n\n"+err);
					});
					break;
				}
			}
		}else{
			Cy.make_modal(window.initials, obj);
		}
	}
};

console.warn("--------------- RUN script >"+Cy.is);

if(Cy.is=="PH"){
	if(Cy.G.loc.indexOf("embed/")>0||Cy.G.loc.indexOf("view_video.php")>0){
		let vid = 0;
		let vars = null;
		if(typeof(VIDEO_SHOW)!="undefined")
			vid = ~~(VIDEO_SHOW.trackVideoId||VIDEO_SHOW.video_id);
		if(vid>0){
			console.log("vid: "+vid);
			if(window.hasOwnProperty("flashvars_"+vid)){
				vars = window["flashvars_"+vid];
			}
		}
		if(vars==null){// try find it in window scope
			for(let k in window) if(k.startsWith("flashvars")) vars=window[k];
		}
		if(vars!=null){
			console.log("vars: "+JSON.stringify(vars));
			if(vars.hasOwnProperty("mediaDefinitions")){
				Cy.handler(vars);
			}else alert("'flashvars' found but not contains 'mediaDefinitions'");
		}else{// sandbox?
			let ta=document.createElement("textarea");
			ta.id="cyru55";
			ta.value="const Cy = "+O2S(Cy)+`;

				let vid = 0;
				let vars = null;
				if(typeof(VIDEO_SHOW)!="undefined")
					vid = ~~(VIDEO_SHOW.trackVideoId||VIDEO_SHOW.video_id);
				if(vid>0){
					console.log("vid: "+vid);
					if(window.hasOwnProperty("flashvars_"+vid)){
						vars = window["flashvars_"+vid];
					}
				}
				if(vars==null){// try find it in window scope
					for(let k in window) if(k.startsWith("flashvars")) vars=window[k];
				}
				if(vars!=null){
					console.log("vars: "+JSON.stringify(vars));
					if(vars.hasOwnProperty("mediaDefinitions")){
						Cy.handler(vars);
					}else alert("'flashvars' found but not contains 'mediaDefinitions'");
				}else alert("'flashvars' not found");
				Cy.el.del("cyru55");
			`;
			document.body.appendChild(ta);
			document.body.setAttribute("onload",'window.eval(document.getElementById("cyru55").value);');
		}
	}
	// maximize view with removing right panel
	Cy.el.del("hd-rightColVideoPage");
	let el=Cy.el.id("vpContentContainer");
	if(el) el.style.display="block";
	// bigger watched flag
	let style1 = document.createElement("style");
	style1.innerText = "div.watchedVideoText{ line-height:32px;font-size:32px; }";
	document.head.appendChild(style1);
	// remove adblock blockers
	for(let i=16;i;) setTimeout( ()=>Cy.ph_block_adblock_blockers(), 200*--i);
	// remove cookie banner
	Cy.el.del("cookieBanner");
	document.cookie = "cookieConsent=3; expires=0;domain=pornhub.com;secure;path=/";
	cookieConsentUserChoice("granted");
	logUserConsentCookie(3);
	// set language = en
	if(typeof(setCookieAdvanced)!="undefined")
		setCookieAdvanced("lang","en", 30, "/", removeSubdomain(window.location.host));
	// may set backend dont-show-ads
	if(typeof(MG_Utils)!="undefined")
		MG_Utils.ajaxCall({
			type: "GET",
			url: "/front/cookie_kill_ajax",
			data: {cookie_name: "showPremiumWelcomeFromPornhub"}
		});
	// try set custom settings
	Cy.ph_change_params();
	// remove age warning
	//Cy.ph_rm_age_nag();
}else{
	if(
		window.hasOwnProperty("initials")&&
		!window.location.pathname.indexOf("/videos/")
	){
		let mp4 = initials?.videoModel?.sources?.mp4?? initials?.xplayerSettings?.sources?.standard?.h264;
		if(mp4){
			Cy.handler(mp4);
		}else alert("it seems website scripts changed\ncontact me @cyru55");
		setInterval( ()=>{
			let ads = document.querySelectorAll("div.thumb-list>div:not(.thumb-list__item)");
			for(let ad of ads) Cy.el.del(ad);
		}, 200);
		let css = document.createElement("style");
		css.innerText = ".player-container{margin-right:unset !important}";
		document.getElementsByTagName('head')[0].appendChild(css);
	}
}