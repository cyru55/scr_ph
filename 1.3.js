// ==UserScript==
// @name         PH Downloader
// @version      1.3
// @description  download videos
// @author       cyru55
// @match        https://*.pornhub.com/*
// @run-at       document-end
// @grant        none
// @license      WTFPL
// @downloadURL  https://gitlab.com/cyru55/scr_ph/-/raw/one/latest.js
// @updateURL    https://gitlab.com/cyru55/scr_ph/-/raw/one/latest.js
// ==/UserScript==

function O2S(n){var o=[];if(null==n)return String(n);if("object"==typeof n&&null==n.join){for(prop in n)n.hasOwnProperty(prop)&&o.push(prop+": "+O2S(n[prop]));return"{"+o.join(",")+"}"}if("object"==typeof n&&null!=n.join){for(prop in n)o.push(O2S(n[prop]));return"["+o.join(",")+"]"}return"function"==typeof n?o.push(n.toString()):o.push(JSON.stringify(n)),o.join(",")}

const Cy = {
	G:{
		loc: window.location.href
	},
	el:{
		id: str => document.getElementById(str),
		cls: str => document.getElementsByClassName(str),
		q: str => document.querySelectorAll(str),
		q1: str => document.querySelector(str),
		del: el => {
			if(typeof(el)=="string") el=Cy.el.id(el);
			if(el) el.parentNode.removeChild(el);
		}
	},
	block_adblock_blockers: ()=>{
		if(typeof(page_params)!="undefined")
			page_params.isAdBlockEnable=false;
		if(typeof(setCookieAdvanced)=="function")
			setCookieAdvanced("adBlockAlertHidden",1,1,"/","pornhub.com");
		Cy.el.del("abAlert");
		if(typeof(MG_Utils)!="undefined")
			MG_Utils.removeClass(Cy.el.id("header"),"hasAdAlert");
		page_params.isAdBlockEnable=false;
	},
	rm_age_nag: ()=>{
		Cy.el.del("ageDisclaimerMainBG");
		Cy.el.del("ageDisclaimerOverlay");
		Cy.el.del("js-ageDisclaimerModal");
		Cy.el.del("modalWrapMTubes");
		if(typeof(setCookieAdvanced)=="function")
			setCookieAdvanced("accessAgeDisclaimerPH",1,1,"/","pornhub.com");
	},
	change_params: ()=>{
		for(var i=32;i;){
			setTimeout( ()=>{
				if(typeof(essentialCookiesListAll)!="undefined"){
					essentialCookiesListAll["pornhub.com"]["CookieConsent"]=3;
					essentialCookiesListAll["pornhub.com"]["cookieConsent"]=3;
					essentialCookiesListAll["pornhub.com"]["accessAgeDisclaimerPH"]=1;
					essentialCookiesListAll["pornhub.com"]["accessAgeDisclaimerUK"]=1;
					essentialCookiesListAll["pornhub.com"]["accessPH"]=1;
					essentialCookiesListAll["pornhub.com"]["adBlockAlertHidden"]=1;
					essentialCookiesListAll["pornhub.com"]["age_verified"]=1;
					essentialCookiesListAll["pornhub.com"]["cookieBannerEU"]=1;
					essentialCookiesListAll["pornhub.com"]["cookieBannerState"]=1;
					essentialCookiesListAll["pornhub.com"]["cookiesBanner"]=1;
					essentialCookiesListAll["pornhub.com"]["cookiesBannerSeen"]=1;
					essentialCookiesListAll["pornhub.com"]["autoplay"]=0;
				}
			},500*--i);
		}
	},
	to_list: function(obj,json){
		var out="";
		if(json.length){
			var title=obj.video_title
				.replace(/'s/ig,"")
				.replace(/[^\w\s\-\+\.,]/g,"")
				.replace(/([\-\+\.,]){2,}/g,"$1")
				.replace(/\s+/g," ")
				.replace(/^[\-\+\.\s_,]+/,"")
				.replace(/[\-\+\.\s_,]+$/,"")
				.replace(/\W*S\d+E\d+\W*/,"");
			title=title.substr(0,232);
			out = '<div style="text-align:center;"><a href="javascript:(()=>{ cymod.style.display=\'none\'; })();" style="color:#f00;font-size:99px;">x</a></div>';
			if(Cy.G.loc.indexOf("embed/")!=-1){
				out+='<div><a target="_blank" href="'+Cy.G.loc.replace("embed/","view_video.php?viewkey=")+'">🟡 High Resolution in New Tab</a></div><br>';
			}
			var vhex = Cy.G.loc.substr(Cy.G.loc.indexOf("=")+1);
			for(var i in json){
				if(json[i].format=="mp4"){
					out+='<div>'+
						'<a target="dl" download="'+title+(vhex.length>3?" ,PH"+vhex:"")+'.mp4" href="'+json[i].videoUrl+'">⬇️ '+json[i].quality+'</a>'+
						'<button onclick="(()=>{ try{navigator.clipboard.writeText(\''+json[i].videoUrl+'\');}catch(e){} })();" style="margin-left:32px;padding:8px;">📎</button>'+
					'</div>';
				}
			}
			if(vhex.length>3)
				out+='<br><input type="text" value="PH_'+vhex+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
			out+='<br><input type="text" value="'+title+(vhex.length>3?" ,PH"+vhex:"")+'" style="width:96%;font-size:20px;" onclick="((_)=>{_.select();try{navigator.clipboard.writeText(_.value);}catch(e){};})(this);"/>';
		}
		return out;
	},
	make_modal: function(obj,json){
		var html = Cy.to_list(obj,json);
		if(html.length){
			var div1 = document.createElement("div");
			div1.id = "cymod";
			div1.style.cssText =
				'position:fixed;'+
				'z-index:999;'+
				'top:50%;'+
				'left:50%;'+
				'transform:translate(-50%,-70%);'+
				'width:320px;'+
				'width:60vw;'+
				'background:#000;'+
				'background:#000c;'+
				'padding:40px;'+
				'padding-top:0;'+
				'color:blue;'+
				'border-radius:44px;'+
				'box-shadow:0 0 44px 22px #000;'+
				'font-size:32px;';
			div1.innerHTML = html;
			document.body.appendChild(div1);
		}
	},
	handler: function(obj){
		for(var i in obj.mediaDefinitions){
			if(obj.mediaDefinitions[i].format=="mp4"){
				fetch(
					obj.mediaDefinitions[i].videoUrl,
					{mode:"cors"}
				)
				.then(res => {
					if(res.ok)
						return res.json();
					throw("server returns non2xx");
				})
				.then(json => {
					Cy.make_modal(obj,json);
				})
				.catch(err => {
					alert("Error while api request\n\n"+err);
				});
				break;
			}
		}
	}
};


if(Cy.G.loc.indexOf("embed/")>0||Cy.G.loc.indexOf("view_video.php")>0){
	var vid = 0;
	var vars = null;
	if(typeof(VIDEO_SHOW)!="undefined")
		vid = ~~(VIDEO_SHOW.trackVideoId||VIDEO_SHOW.video_id);
	if(vid>0){
		console.log("vid: "+vid);
		if(window.hasOwnProperty("flashvars_"+vid)){
			vars = window["flashvars_"+vid];
		}
	}
	if(vars==null){// try find it in window scope
		for(var k in window) if(k.startsWith("flashvars")) vars=window[k];
	}
	if(vars!=null){
		console.log("vars: "+JSON.stringify(vars));
		if(vars.hasOwnProperty("mediaDefinitions")){
			Cy.handler(vars);
		}else alert("'flashvars' found but not contains 'mediaDefinitions'");
	}else{// sandbox?
		var ta=document.createElement("textarea");
		ta.id="cyru55";
		ta.value="const Cy = "+O2S(Cy)+`;

			var vid = 0;
			var vars = null;
			if(typeof(VIDEO_SHOW)!="undefined")
				vid = ~~(VIDEO_SHOW.trackVideoId||VIDEO_SHOW.video_id);
			if(vid>0){
				console.log("vid: "+vid);
				if(window.hasOwnProperty("flashvars_"+vid)){
					vars = window["flashvars_"+vid];
				}
			}
			if(vars==null){// try find it in window scope
				for(var k in window) if(k.startsWith("flashvars")) vars=window[k];
			}
			if(vars!=null){
				console.log("vars: "+JSON.stringify(vars));
				if(vars.hasOwnProperty("mediaDefinitions")){
					Cy.handler(vars);
				}else alert("'flashvars' found but not contains 'mediaDefinitions'");
			}else alert("'flashvars' not found");
			Cy.el.del("cyru55");
		`;
		document.body.appendChild(ta);
		document.body.setAttribute("onload",'window.eval(document.getElementById("cyru55").value);');
	}
	// maximize view with removing right panel
	Cy.el.del("hd-rightColVideoPage");
	var el=Cy.el.id("vpContentContainer");
	if(el) el.style.display="block";
}
console.warn("--------------- RUN scr_ph");

// bigger watched flag
var style1 = document.createElement("style");
style1.innerText = "div.watchedVideoText{ line-height:32px;font-size:32px; }";
document.head.appendChild(style1);

// remove adblock blockers
for(var i=16;i;) setTimeout( ()=>Cy.block_adblock_blockers(), 200*--i);

// remove cookie banner
Cy.el.del("cookieBanner");
//document.cookie = cbCookieName + "=3; expires=" + cbExpiration + ";domain=" + cbDomainName + ";secure;path=/";
document.cookie = "cookieConsent=3; expires=0;domain=pornhub.com;secure;path=/";
cookieConsentUserChoice("granted");
logUserConsentCookie(3);

// set language = en
if(typeof(setCookieAdvanced)!="undefined")
	setCookieAdvanced("lang","en", 30, "/", removeSubdomain(window.location.host));

// may set backend dont-show-ads
if(typeof(MG_Utils)!="undefined")
	MG_Utils.ajaxCall({
		type: "GET",
		url: "/front/cookie_kill_ajax",
		data: {cookie_name: "showPremiumWelcomeFromPornhub"}
	});

// try set custom settings
Cy.change_params();

// remove age warning
//Cy.rm_age_nag();
