// ==UserScript==
// @name         PH Downloader
// @version      0.1
// @description  download videos
// @author       cyru55
// @match        https://*.pornhub.com/*
// @grant        none
// ==/UserScript==

(function(){

	window["Cy"] = {
		el:{
			id: str => document.getElementById(str),
			del: el => {if(typeof(el)=="string") el=Cy.el.id(el);el.parentNode.removeChild(el)}
		},
		block_adblock_blockers: function(){
			// hey PH, plz do not block "block ad-block blockers"
			if(typeof(page_params)!="undefined")
				page_params.isAdBlockEnable = false;
			if(typeof(setCookieAdvanced)=="function")
				setCookieAdvanced('adBlockAlertHidden',1,1);
			Cy.el.del("abAlert");
		},
		modal: function(obj,json){
			var html="";
			for(var i in json){
				if(json[i].format=="mp4"){
					html+='<br><div class="categoriesWrapper"><a target="dl" download="'+obj.video_title+'.mp4" style="font-size:32px;" href="'+json[i].videoUrl+'">'+json[i].quality+'</a></div>';
				}
			}
			if(html.length){
				var div = document.createElement('div');
				div.innerHTML = '<div style="position:fixed;z-index:999;top:50%;left:50%;transform:translate(-50%,-50%);width:320px;width:60vw;background:#fce;padding:40px;color:blue;" id="cyru55" onclick="setTimeout(function(){Cy.el.del(\'cyru55\');},500)">'+html+'</div>';
				document.body.appendChild(div);
			}
		},
		handler: function(obj){
			for(var i in obj.mediaDefinitions){
				if(obj.mediaDefinitions[i].format=="mp4"){
					fetch(
						obj.mediaDefinitions[i].videoUrl,
						{mode:"cors"}
					)
					.then(response => {
						if(response.ok && response.status==200){
							return response.json();
						}
					})
					.then( data => {
						Cy.modal(obj,data);
					})
					.catch(err => {
						alert("Error:\n"+err);
					});
					break;
				}
			}
		}
	};

	var i1 = setInterval( () => {

		try{
			Cy.block_adblock_blockers();
		}catch(e){}

		var vid=VIDEO_SHOW.trackVideoId||VIDEO_SHOW.video_id||window["VIDEO_SHOW"]["video_id"];
		var flashvars=window["flashvars_"+vid];
		if(flashvars.hasOwnProperty("mediaDefinitions")){
			Cy.handler(flashvars);
			clearInterval(i1);
		}

	},100);

})();
